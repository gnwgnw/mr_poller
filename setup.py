from setuptools import setup, find_packages

setup(
    name='mr_poller',
    version='0.3.22',
    description="Poller for MR in GitLab PREP repo",
    url='https://gitlab.com/tp-prep/test',
    author='Serg Titaevskiy',
    author_email='titaevskiy.s@gmail.com',
    license='BSD3',
    packages=find_packages(),
    install_requires=[
        'python-gitlab',
        'cached-property',
        'SQLAlchemy',
        'psycopg2-binary',
        'alembic',
        'prettytable',
        'fire',
    ],
    entry_points={
        'console_scripts': [
            'mr-poller=mr_poller.command_line:main',
            'mr-poller-dev=mr_poller.command_line:run',
        ],
    },
)
