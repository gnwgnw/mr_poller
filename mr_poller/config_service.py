import copy
import logging
import os
import shlex

from mr_poller import utils

logger = logging.getLogger(__name__)


class NoEnvVarException(Exception):
    pass


class EnvVarGetter(object):
    def __init__(self, name, var_type=str):
        self._name = name
        self._type = var_type

    def get(self):
        if self._name in os.environ:
            return self._type(os.environ[self._name])

        raise NoEnvVarException(self._name)


class EnvVarListGetter(EnvVarGetter):
    def get(self):
        env = super(EnvVarListGetter, self).get()
        return [self._type(val) for val in shlex.split(env)]


_GETTERS = [
    ('debug', EnvVarGetter('DEBUG', bool)),
    ('db_url', EnvVarGetter('DATABASE_URL')),
    ('secret_token', EnvVarGetter('MRP_SECRET_TOKEN')),
    ('project_name', EnvVarGetter('MRP_PROJECT_NAME')),
    ('last_mr_id', EnvVarGetter('MRP_LAST_MR_ID', int)),
    ('reviewers', EnvVarListGetter('MRP_REVIEWERS')),
    ('mentors', EnvVarListGetter('MRP_MENTORS')),
    ('mini_mentors', EnvVarListGetter('MRP_MINI_MENTORS')),
    ('cache_timeout', EnvVarGetter('MRP_CACHE_TIMEOUT', int)),
]


@utils.singleton
class ConfigService(object):
    def __init__(self):
        self._config = {}
        for k, getter in _GETTERS:
            self._config[k] = getter.get()

        cfg = copy.deepcopy(self._config)
        cfg.pop('secret_token')

        logger.debug("Configs: {}".format(cfg))

    @property
    def config(self):
        return self._config

    @property
    def reviewers(self):
        return self.config['reviewers']

    @property
    def secret_token(self):
        return self.config['secret_token']

    @property
    def project_name(self):
        return self.config['project_name']

    @property
    def last_mr_id(self):
        return self.config['last_mr_id']

    @property
    def db_url(self):
        return self.config['db_url']

    @property
    def mentors(self):
        return self.config['mentors']

    @property
    def mini_mentors(self):
        return self.config['mini_mentors']

    @property
    def debug(self):
        return self.config['debug']

    @property
    def cache_timeout(self):
        return self.config['cache_timeout']
