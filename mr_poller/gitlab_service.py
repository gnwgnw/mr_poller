import itertools
import logging

import gitlab
from cached_property import cached_property, timed_cached_property

from mr_poller import utils
from mr_poller.config_service import ConfigService
from mr_poller.db_service import DBService

logger = logging.getLogger(__name__)

MINI_MENTOR_REVIEW_LABEL = 'mini_mentor_review'
MINI_MENTOR_REVIEW_DONE_LABEL = 'mini_mentor_review_done'
MENTOR_REVIEW_LABEL = 'mentor_review'
MENTOR_REVIEW_DONE_LABEL = 'mentor_review_done'
TUTOR_REVIEW_LABEL = 'tutor_review'

LABELS = [
    {'name': MINI_MENTOR_REVIEW_LABEL, 'color': '#639fff'},
    {'name': MINI_MENTOR_REVIEW_DONE_LABEL, 'color': '#75faa6'},
    {'name': MENTOR_REVIEW_LABEL, 'color': '#5148f7'},
    {'name': MENTOR_REVIEW_DONE_LABEL, 'color': '#7eff4f'},
    {'name': TUTOR_REVIEW_LABEL, 'color': '#ff94ed'},
    {'name': 'need_rework', 'color': '#fcbe42'},
    {'name': 'reworked', 'color': '#c064fa'},
    {'name': '11/10', 'color': '#f74a81'},
]


@utils.singleton
class GitLabService(object):
    def __init__(self):
        self._config = ConfigService()
        self._db = DBService()

        self._gl = gitlab.Gitlab(
            'https://gitlab.com', private_token=self._config.secret_token,
        )
        self._gl.auth()

    @staticmethod
    def _usernames(users):
        return [user.username for user in users]

    def _get_gl_users(self, user_names):
        return [self._gl.users.list(search=name)[0] for name in user_names]

    @cached_property
    def mini_mentor_users(self):
        logger.info("Fetching mini-mentors")
        return self._get_gl_users(self._config.mini_mentors)

    @cached_property
    def mini_mentor_usernames(self):
        return self._usernames(self.mini_mentor_users)

    @cached_property
    def mentor_users(self):
        logger.info("Fetching mentors")
        return self._get_gl_users(self._config.mentors)

    @cached_property
    def mentor_usernames(self):
        return self._usernames(self.mentor_users)

    @cached_property
    def reviewer_users(self):
        logger.info("Fetching tutors")
        return self._get_gl_users(self._config.reviewers)

    @cached_property
    def reviewer_usernames(self):
        return self._usernames(self.reviewer_users)

    @cached_property
    def mentor_pool_users(self):
        return self.mentor_users + self.mini_mentor_users

    @cached_property
    def mentor_pool_usernames(self):
        return self._usernames(self.mentor_pool_users)

    @staticmethod
    def _get_gl_users_cycle(users, last_user_name):
        iter_users = itertools.cycle(users)

        if last_user_name in [user.username for user in users]:
            user = next(iter_users)
            while user.username != last_user_name:
                user = next(iter_users)

        return iter_users

    @cached_property
    def reviewers_cycle(self):
        logger.debug(
            "Active reviewers: {}".format(self.reviewer_usernames)
        )

        return self._get_gl_users_cycle(self.reviewer_users, self._db.last_reviewer_username)

    @cached_property
    def mentors_cycle(self):
        logger.debug(
            "Active mentors: {}".format(self.mentor_usernames)
        )

        return self._get_gl_users_cycle(self.mentor_users, self._db.last_mentor_username)

    @cached_property
    def mentor_pool(self):
        logger.debug(
            "Active mentor pool: {}".format(self.mentor_pool_usernames)
        )

        return self._get_gl_users_cycle(self.mentor_pool_users, self._db.last_mentor_from_pool_username)

    def get_projects(self, visibility='private'):
        logger.info("Fetching projects")

        raw_projects = self._gl.projects.list(
            membership=True, all=True, search=self._config.project_name, visibility=visibility,
        )

        projects = []
        for project in raw_projects:
            if all([member.access_level >= gitlab.DEVELOPER_ACCESS for member in project.members.list()]):
                projects.append(project)
            else:
                logger.warning("Access problems with {}".format(project.web_url))

        logger.info("Projects count: {}".format(len(projects)))

        return projects

    @cached_property
    def _projects(self):
        return timed_cached_property(self._config.cache_timeout)(self.get_projects.__func__)

    @property
    def projects(self):
        return self._projects.__get__(self, self.__class__)

    def update_projects_labels(self):
        logger.info("Update projects labels")

        for p in self.projects:
            project_label_names = [l.name for l in p.labels.list()]

            for label in LABELS:
                if label['name'] not in project_label_names:
                    p.labels.create(label)
                    logging.info("Add {} label in {}".format(label, p.web_url))

    def get_merge_requests(self, state='opened', labels=None):
        logger.info("Fetching MRs, state={}, labels={}".format(state, labels))

        if labels is None:
            labels = []

        mrs = []
        for p in self.projects:
            mrs.extend(p.mergerequests.list(state=state, labels=labels))

        mrs.sort(key=lambda m: m.id)

        return mrs

    @property
    def mr_to_mentor_pool(self):
        def filter_by_labels(mr):
            for label in LABELS:
                if label['name'] in mr.labels:
                    return False
            return True

        return [mr for mr in self.get_merge_requests()
                if (filter_by_labels(mr) and mr.id > self._db.last_mr_id)]

    @property
    def mr_to_mentors(self):
        return self.get_merge_requests(labels=[MINI_MENTOR_REVIEW_DONE_LABEL])

    @property
    def mr_to_tutors(self):
        return self.get_merge_requests(labels=[MENTOR_REVIEW_DONE_LABEL])

    def update_mentor_pool(self):
        logger.info("Update mentor pool")

        for mr in self.mr_to_mentor_pool:
            next_mentor = next(self.mentor_pool)
            logger.info("Assign pool mentor {} to MR {}".format(next_mentor.username, mr.web_url))

            mr.assignee_id = next_mentor.id

            if next_mentor.username in self.mini_mentor_usernames:
                mr.labels = [MINI_MENTOR_REVIEW_LABEL]
            else:
                mr.labels = [MENTOR_REVIEW_LABEL]

            if not self._config.debug:
                mr.save()

            logger.info("Set conf: last MR id {}, last pool mentor {}".format(mr.id, next_mentor.username))

            self._db.last_mentor_from_pool_username = next_mentor.username
            self._db.last_mr_id = mr.id

    def update_mentors(self):
        logger.info("Update mentors")

        for mr in self.mr_to_mentors:
            next_mentor = next(self.mentors_cycle)
            logger.info("Assign mentor {} to MR {}".format(next_mentor.username, mr.web_url))

            mr.assignee_id = next_mentor.id
            mr.labels = [MENTOR_REVIEW_LABEL]
            if not self._config.debug:
                mr.save()

            logger.info("Set conf: last MR id {}, last mentor {}".format(mr.id, next_mentor.username))

            self._db.last_mentor_username = next_mentor.username

    def update_tutors(self):
        logger.info("Update tutors")

        for mr in self.mr_to_tutors:
            next_tutor = next(self.reviewers_cycle)
            logger.info("Assign tutor {} to MR {}".format(next_tutor.username, mr.web_url))

            mr.assignee_id = next_tutor.id
            mr.labels = [TUTOR_REVIEW_LABEL]
            if not self._config.debug:
                mr.save()

            logger.info("Set conf: last tutor {}".format(next_tutor.username))

            self._db.last_reviewer_username = next_tutor.username

    def update_assignee(self):
        self.update_mentor_pool()
        self.update_mentors()
        self.update_tutors()
