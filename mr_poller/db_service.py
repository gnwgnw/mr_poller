import logging

import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker

from mr_poller import utils
from mr_poller.config_service import ConfigService
from mr_poller.models import Config

logger = logging.getLogger(__name__)


@utils.singleton
class DBService(object):
    def __init__(self):
        config_service = ConfigService()

        engine = sa.create_engine(config_service.db_url)
        self._session = sessionmaker(bind=engine)()

        conf = self._session.query(Config).first()
        if conf is None:
            mentor_pool = config_service.mentors + config_service.mini_mentors
            conf = Config(
                last_mr_id=config_service.last_mr_id,
                last_reviewer_username=config_service.reviewers[-1],
                last_mentor_username=config_service.mentors[-1],
                last_mentor_from_pool_username=mentor_pool[-1],
            )
            self._session.add(conf)
            self._session.commit()

        if config_service.last_mr_id != 0:
            conf.last_mr_id = config_service.last_mr_id
            self._session.commit()

        self._conf = conf

        logger.debug("Configs: {}".format(self._conf))

    @property
    def last_mr_id(self):
        return self._conf.last_mr_id

    @last_mr_id.setter
    def last_mr_id(self, value):
        self._conf.last_mr_id = value
        self._session.commit()

    @property
    def last_reviewer_username(self):
        return self._conf.last_reviewer_username

    @last_reviewer_username.setter
    def last_reviewer_username(self, value):
        self._conf.last_reviewer_username = value
        self._session.commit()

    @property
    def last_mentor_username(self):
        return self._conf.last_mentor_username

    @last_mentor_username.setter
    def last_mentor_username(self, value):
        self._conf.last_mentor_username = value
        self._session.commit()

    @property
    def last_mentor_from_pool_username(self):
        return self._conf.last_mentor_from_pool_username

    @last_mentor_from_pool_username.setter
    def last_mentor_from_pool_username(self, value):
        self._conf.last_mentor_from_pool_username = value
        self._session.commit()
