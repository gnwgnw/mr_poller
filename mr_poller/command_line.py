import logging

import fire
from prettytable import PrettyTable

from mr_poller.gitlab_service import GitLabService

logger = logging.getLogger(__name__)


class MrPollerCLI(object):
    def __init__(self):
        self._gl = GitLabService()

    def update_assignee(self, ):
        self._gl.update_assignee()

    def list_repos(self, visibility='private'):
        t = PrettyTable(['User', 'url'])
        for project in self._gl.get_projects(visibility=visibility):
            owner = ''
            if hasattr(project, 'owner'):
                owner = project.owner['username']
            t.add_row([owner, project.web_url])
        print(t)

    def stats(self):
        states = ['opened', 'merged', 'closed']
        t = PrettyTable(['#HW'] + states)

        mrs = self._gl.get_merge_requests(state=None)
        for i in range(1, 8):
            branch_name = 'hw-{}'.format(i)
            row = []
            for state in states:
                state_mrs = [mr for mr in mrs if mr.state == state]
                hw_state_mrs = [
                    mr for mr in state_mrs
                    if (branch_name in mr.target_branch) or
                       (branch_name in mr.source_branch)
                ]
                row.append(str(len(hw_state_mrs)))

            t.add_row([branch_name] + row)

        print(t)


def run():
    fire.Fire(MrPollerCLI)


def main():
    gl = GitLabService()
    gl.update_projects_labels()
    gl.update_assignee()


if __name__ == '__main__':
    main()
