import sqlalchemy as sa

from .base import Base


class Config(Base):
    __tablename__ = 'configs'

    last_mr_id = sa.Column(sa.Integer, nullable=False)
    last_reviewer_username = sa.Column(sa.String)
    last_mentor_username = sa.Column(sa.String)
    last_mentor_from_pool_username = sa.Column(sa.String)

    def __str__(self):
        return str({
            'last_mr_id': self.last_mr_id,
            'last_reviewer_username': self.last_reviewer_username,
            'last_mentor_username': self.last_mentor_username,
            'last_mentor_from_pool_username': self.last_mentor_from_pool_username,
        })
