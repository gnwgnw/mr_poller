import sqlalchemy as sa
from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class Base(object):
    id = sa.Column(sa.Integer, primary_key=True)
