[![pipeline status](https://gitlab.com/mrGexogen/mr_poller/badges/master/pipeline.svg)](https://gitlab.com/mrGexogen/mr_poller/commits/master)

# Распределение MR'ов

mini mentor - неопытные менторы-новички

mentor - чистокровные и опытные менторы

mentor pool - пул менторов и минименторов
tutor/reviewer - преподаватель

Сначала MR попадает в пул, где может быть проверен либо ментором, либо миниментором.
Если MR был проверен миниментором, то его также должен проверить ментор.

MR'ы, проверенные менторами сразу идут на проверку к преподавателям

